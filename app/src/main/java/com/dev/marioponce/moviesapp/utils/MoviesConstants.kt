package com.dev.marioponce.moviesapp.utils

object MoviesConstants {
    const val API_KEY = "93aea0c77bc168d8bbce3918cefefa45"
    const val MOVIES_IMAGE_URL = "https://image.tmdb.org/t/p/w500"
    const val MOVIES_DB_SEARCH_URL = "https://api.themoviedb.org/4/search/movie?api_key=93aea0c77bc168d8bbce3918cefefa45&language=en-US&"
    const val MOVIES_DB_DISCOVER_URL = "https://api.themoviedb.org/4/discover/movie?api_key=93aea0c77bc168d8bbce3918cefefa45&language=en-US&"
    const val MOVIES_DB_SEARCH = MOVIES_DB_SEARCH_URL + "query="
    const val MOVIES_DB_DISCOVER = MOVIES_DB_DISCOVER_URL + "sort_by=popularity.desc&include_adult=false&include_video=true&"
    const val MOVIES_DB_PAGE = "&page="
    const val REQUEST_OK = "REQUEST OK: "
    const val REQUEST_KO = "REQUEST KO: "
    const val EXTRA_MOVIE = "movie"
}