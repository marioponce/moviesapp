package com.dev.marioponce.moviesapp.repository.model

import com.dev.marioponce.moviesapp.utils.RequestType

data class MoviesQueryEntry(
    var page: Int,
    var requestType: RequestType,
    var query: String = ""
)
