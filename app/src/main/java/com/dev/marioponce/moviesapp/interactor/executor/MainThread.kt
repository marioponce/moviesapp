package com.dev.marioponce.moviesapp.interactor.executor

interface MainThread {
    fun post(runnable: Runnable)
}