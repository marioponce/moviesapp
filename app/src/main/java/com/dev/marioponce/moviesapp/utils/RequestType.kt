package com.dev.marioponce.moviesapp.utils

enum class RequestType {
    SEARCH,
    DISCOVER
}