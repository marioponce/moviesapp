package com.dev.marioponce.moviesapp.utils

import com.google.gson.GsonBuilder

abstract class MoviesBaseRepository {
    val gson = GsonBuilder().setDateFormat("M/d/yy hh:mm a").create()
}