package com.dev.marioponce.moviesapp.presenter.impl

import android.content.Context
import com.dev.marioponce.moviesapp.interactor.MoviesInteractor
import com.dev.marioponce.moviesapp.interactor.impl.MoviesInteractorImpl
import com.dev.marioponce.moviesapp.presenter.MoviesPresenter
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.repository.model.MoviesQueryEntry
import com.dev.marioponce.moviesapp.repository.model.Result
import java.util.ArrayList

class MoviesPresenterImpl : MoviesPresenter, MoviesInteractor.MoviesInteractorCallback {

    private lateinit var view : MoviesPresenter.View

    override fun setView(view: MoviesPresenter.View) {
        this.view = view
    }

    /**
     * UI called this method to get the movie List
     *
     */
    override fun callDiscoverMoviesService(entry: MoviesQueryEntry, context: Context) {
        val interactor = MoviesInteractorImpl(entry, context)
        interactor.execute(this)
    }

    override fun onSuccess(result: ApiMoviesResponse) {
        view.onSuccess(result)
    }

    override fun onError() {
        view.onError()
    }
}