package com.dev.marioponce.moviesapp.presenter

import android.content.Context
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.repository.model.MoviesQueryEntry
import com.dev.marioponce.moviesapp.repository.model.Result
import java.util.ArrayList

interface MoviesPresenter {

    fun setView(view: View)
    fun callDiscoverMoviesService(entry: MoviesQueryEntry, context: Context)

    interface View {
        fun onSuccess(result: ApiMoviesResponse)
        fun onError()
    }

}