package com.dev.marioponce.moviesapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.transition.TransitionManager
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.dev.marioponce.moviesapp.R
import com.dev.marioponce.moviesapp.presenter.MoviesPresenter
import com.dev.marioponce.moviesapp.presenter.impl.MoviesPresenterImpl
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.repository.model.MoviesQueryEntry
import com.dev.marioponce.moviesapp.repository.model.Result
import com.dev.marioponce.moviesapp.ui.adapter.MoviesAdapter
import com.dev.marioponce.moviesapp.utils.MoviesConstants.EXTRA_MOVIE
import com.dev.marioponce.moviesapp.utils.RequestType
import kotlinx.android.synthetic.main.activity_movies.*

class MoviesActivity : AppCompatActivity(), MoviesPresenter.View, MoviesAdapter.MoviesAdapterListener {

    private val presenter: MoviesPresenterImpl = MoviesPresenterImpl()
    private var moviesResponse: ApiMoviesResponse? = null
    private var moviesAdapter: MoviesAdapter? = null
    private lateinit var actualMoviesQuery: MoviesQueryEntry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        setSupportActionBar(toolbar)
        presenter.setView(this)
        callDiscoverMoviesService(MoviesQueryEntry(1, RequestType.DISCOVER))
    }

    /**
     * This method prepare the Search View to enable the search function
     *
     */
    private fun configureSearchView(menu: Menu) {
        val search: SearchView = menu.findItem(R.id.action_search).actionView as SearchView
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                moviesResponse = null
                if (newText.isEmpty())
                    callDiscoverMoviesService(MoviesQueryEntry(1, RequestType.DISCOVER))
                else
                    callDiscoverMoviesService(MoviesQueryEntry(1, RequestType.SEARCH, newText))

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        configureSearchView(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_search -> {
            TransitionManager.beginDelayedTransition(toolbar)
            MenuItemCompat.expandActionView(item)
        }
        else -> super.onOptionsItemSelected(item)
    }

    /**
     * Init the Movies recycler
     *
     */
    private fun initRecycler() {
        moviesAdapter = MoviesAdapter(this.moviesResponse!!, this, this)
        movies_recycler.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        dividerItemDecoration.setDrawable(getDrawable(R.drawable.movies_divider)!!)
        movies_recycler.addItemDecoration(dividerItemDecoration)
        movies_recycler.layoutManager = layoutManager
        movies_recycler.adapter = moviesAdapter
    }

    private fun updateAdapter() {
        moviesAdapter!!.updateAdapter(this.moviesResponse!!)
    }

    /**
     * This function request to the presenter a movies result.
     * we always save the last entry because whe need it for pagination calls (onBottomReached)
     *
     */
    private fun callDiscoverMoviesService(entry: MoviesQueryEntry) {
        actualMoviesQuery = entry
        presenter.callDiscoverMoviesService(entry, this)
    }


    /**
     * This function is called when the request has been completed correctly
     *
     */
    override fun onSuccess(result: ApiMoviesResponse) {
        movies_recycler.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        error_component.visibility = View.GONE
        if (this.moviesResponse == null) {
            this.moviesResponse = result
            initRecycler()
        } else {
            result.results.addAll(0, this.moviesResponse!!.results)
            this.moviesResponse = result
            updateAdapter()
        }
    }

    /**
     * Called when the the request fail
     *
     */
    override fun onError() {
        progressBar.visibility = View.GONE
        movies_recycler.visibility = View.GONE
        initErrorLayout()
    }

    private fun initErrorLayout() {
        error_component.visibility = View.VISIBLE
        error_icon.setOnClickListener { callDiscoverMoviesService(MoviesQueryEntry(1, RequestType.DISCOVER)) }
    }

    /**
     * Movie item is clicked
     *
     */
    override fun onMovieDetailClicked(movie: Result) {
        val intent = Intent(this, MoviesDetailActivity::class.java)
        intent.putExtra(EXTRA_MOVIE, movie)
        startActivity(intent)
    }

    /**
     * This function is called when reach the bottom of the screen and there is more pages to request.
     *
     */
    override fun onBottomReached(newPage: Int) {
        actualMoviesQuery.page = newPage
        callDiscoverMoviesService(actualMoviesQuery)
    }
}