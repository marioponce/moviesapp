package com.dev.marioponce.moviesapp.repository.model

import java.io.Serializable

data class CreatedBy(
    val gravatar_hash: String,
    val id: String,
    val name: String,
    val username: String
) : Serializable