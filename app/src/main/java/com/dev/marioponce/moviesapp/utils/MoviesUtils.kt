package com.dev.marioponce.moviesapp.utils

import com.dev.marioponce.moviesapp.repository.model.MoviesQueryEntry
import com.dev.marioponce.moviesapp.utils.MoviesConstants.MOVIES_DB_DISCOVER
import com.dev.marioponce.moviesapp.utils.MoviesConstants.MOVIES_DB_PAGE
import com.dev.marioponce.moviesapp.utils.MoviesConstants.MOVIES_DB_SEARCH

class MoviesUtils {

    companion object {

        fun getDateYear(date: String): String {
            if (date.length < 4)
                return date
            return date.substring(0, 4)
        }

        /**
         * This method return an URL formed based en the entry passed as param
         */
        fun getDBMoviesRequest(entry: MoviesQueryEntry): String = when (entry.requestType) {
            RequestType.SEARCH -> getDBMoviesSearchRequest(entry.query, entry.page)
            RequestType.DISCOVER -> getDBMoviesDiscoverRequest(entry.page)
        }

        private fun getDBMoviesSearchRequest(query: String, page: Int): String {
            return MOVIES_DB_SEARCH + query + MOVIES_DB_PAGE + page
        }

        private fun getDBMoviesDiscoverRequest(page: Int): String {
            return MOVIES_DB_DISCOVER + MOVIES_DB_PAGE + page
        }
    }
}