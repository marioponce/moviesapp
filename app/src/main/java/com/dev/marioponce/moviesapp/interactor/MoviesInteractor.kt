package com.dev.marioponce.moviesapp.interactor

import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse

interface MoviesInteractor {
    fun getMoviesWithUrl(url: String)

    interface MoviesInteractorCallback {
        fun onSuccess(result: ApiMoviesResponse)
        fun onError()
    }
}