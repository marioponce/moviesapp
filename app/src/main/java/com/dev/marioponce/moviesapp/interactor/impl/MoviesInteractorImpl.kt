package com.dev.marioponce.moviesapp.interactor.impl

import android.content.Context
import com.dev.marioponce.moviesapp.interactor.MoviesInteractor
import com.dev.marioponce.moviesapp.repository.MoviesRepository
import com.dev.marioponce.moviesapp.repository.impl.MoviesRepositoryImpl
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.repository.model.MoviesQueryEntry
import com.dev.marioponce.moviesapp.utils.MoviesBaseInteractor
import com.dev.marioponce.moviesapp.utils.MoviesUtils

class MoviesInteractorImpl(private val entry: MoviesQueryEntry, val context: Context) : MoviesBaseInteractor(), MoviesInteractor, Runnable, MoviesRepository.ApiMoviesCallback {

    private lateinit var callback: MoviesInteractor.MoviesInteractorCallback
    private val moviesRepositoryImpl = MoviesRepositoryImpl(this, context)

    fun execute(callback: MoviesInteractor.MoviesInteractorCallback) {
        this.callback = callback
        executor.execute(this)                                          // Call run method
    }

    /**
     * Called when Executor is executed
     *
     */
    override fun run() {
        getMoviesWithUrl(MoviesUtils.getDBMoviesRequest(entry))
    }

    override fun getMoviesWithUrl(url: String) {
        moviesRepositoryImpl.callApiMovies(url)
    }

    override fun onSuccess(result: ApiMoviesResponse) {
        callback.onSuccess(result)
    }

    override fun onError() {
        callback.onError()
    }
}