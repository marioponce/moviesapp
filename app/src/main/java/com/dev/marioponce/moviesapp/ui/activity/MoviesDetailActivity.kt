package com.dev.marioponce.moviesapp.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.dev.marioponce.moviesapp.R
import com.dev.marioponce.moviesapp.repository.model.Result
import com.dev.marioponce.moviesapp.utils.MoviesConstants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movies_detail.*

class MoviesDetailActivity : AppCompatActivity() {

    private lateinit var movie: Result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_detail)
        getExtras()
        prepareToolbar()
        setData()
    }

    private fun prepareToolbar() {
        collapsing_toolbar.setExpandedTitleColor(resources.getColor(android.R.color.transparent))
        toolbar_detail.title = getString(R.string.movie_detail)
        setSupportActionBar(toolbar_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setData() {
        Picasso.get().load(MoviesConstants.MOVIES_IMAGE_URL + movie.backdrop_path).into(movie_detail_image)
        movie_detail_title.text = movie.title
        movie_detail_overview.text = movie.overview
        movie_detail_score.text = getString(R.string.movie_score, movie.vote_average.toString())
        movie_detail_date.text = getString(R.string.movie_release, movie.release_date)
    }

    private fun getExtras() {
        val bundle = intent?.extras!!
        if (bundle.containsKey(MoviesConstants.EXTRA_MOVIE))
            movie = bundle.getSerializable(MoviesConstants.EXTRA_MOVIE) as Result
        else
            finish()
    }
}
