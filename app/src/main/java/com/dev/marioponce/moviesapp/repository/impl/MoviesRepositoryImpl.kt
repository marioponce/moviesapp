package com.dev.marioponce.moviesapp.repository.impl

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.dev.marioponce.moviesapp.repository.MoviesRepository
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.utils.MoviesBaseRepository
import com.dev.marioponce.moviesapp.utils.MoviesConstants

class MoviesRepositoryImpl(private val callback: MoviesRepository.ApiMoviesCallback, val context: Context) : MoviesBaseRepository(), MoviesRepository {

    private val TAG = "MoviesRepositoryImpl"
    private val queue = Volley.newRequestQueue(context)


    /**
     * This method make a GET request to Moviedb API with the url passed as param. It's neccesary to instantiate a Volley
     * request. It has a Response listener to know if it's when OK or KO. The result is turned into an ApiMoviesResponse.
     * This object is what we needed to show all the movies in the app.
     *
     */
    override fun callApiMovies(url: String) {
        queue.cancelAll(TAG)
        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                Log.v(TAG, MoviesConstants.REQUEST_OK + response.toString())
                val result = gson.fromJson(response.toString(), ApiMoviesResponse::class.java)
                result?.let { callback.onSuccess(result) }
            },
            Response.ErrorListener {
                Log.v(TAG, MoviesConstants.REQUEST_KO + it.message)
                callback.onError()
            })
        queue.add(stringRequest)
    }
}