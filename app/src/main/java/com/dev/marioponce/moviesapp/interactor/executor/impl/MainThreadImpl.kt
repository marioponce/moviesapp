package com.dev.marioponce.moviesapp.interactor.executor.impl

import android.os.Handler
import com.dev.marioponce.moviesapp.interactor.executor.MainThread
import android.os.Looper

class MainThreadImpl : MainThread {

    private val handler: Handler = Handler(Looper.getMainLooper())

    override fun post(runnable: Runnable) {
        handler.post(runnable)
    }
}