package com.dev.marioponce.moviesapp.repository

import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse

interface MoviesRepository {
    fun callApiMovies(url : String)

    interface ApiMoviesCallback {
        fun onSuccess(result: ApiMoviesResponse)
        fun onError()
    }
}