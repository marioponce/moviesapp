package com.dev.marioponce.moviesapp.ui.adapter

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dev.marioponce.moviesapp.R
import com.dev.marioponce.moviesapp.repository.model.ApiMoviesResponse
import com.dev.marioponce.moviesapp.repository.model.Result
import com.dev.marioponce.moviesapp.utils.MoviesConstants.MOVIES_IMAGE_URL
import com.dev.marioponce.moviesapp.utils.MoviesUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_item.view.*

class MoviesAdapter(
    private var moviesResponse: ApiMoviesResponse, private val context: Context,
    private val listener: MoviesAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val MOVIE_ITEM = 0
    private val PROGRESS_BAR_ITEM = 1

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == PROGRESS_BAR_ITEM)
            return MoviesAdapter.ProgressBarViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.progress_bar_item,
                    viewGroup,
                    false
                )
            )
        return MoviesAdapter.MovieViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.movie_item,
                viewGroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        if (moviesResponse.page == moviesResponse.total_pages || moviesResponse.results.size == 0)
            return moviesResponse.results.size
        return moviesResponse.results.size + 1                                                      // Progress bar added
    }

    override fun getItemViewType(position: Int): Int {
        if (position == moviesResponse.results.size)
            return PROGRESS_BAR_ITEM
        return MOVIE_ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            holder.title.text = moviesResponse.results[position].title
            holder.date.text = MoviesUtils.getDateYear(moviesResponse.results[position].release_date)
            Picasso.get().load(MOVIES_IMAGE_URL + moviesResponse.results[position].poster_path).into(holder.image)
            holder.details.setOnClickListener { listener.onMovieDetailClicked(moviesResponse.results[position]) }
        }
        if (holder is ProgressBarViewHolder) {
            if (moviesResponse.page < moviesResponse.total_pages)
                listener.onBottomReached(moviesResponse.page + 1)
        }
    }

    fun updateAdapter(moviesResponse: ApiMoviesResponse) {
        this.moviesResponse = moviesResponse
        notifyDataSetChanged()
    }

    class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: AppCompatImageView = view.movie_image
        val title: TextView = view.movie_title
        val date: TextView = view.movie_date
        val details: TextView = view.movie_details
    }

    class ProgressBarViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface MoviesAdapterListener {
        fun onBottomReached(newPage: Int)                   // Method called when we want to load more Movies
        fun onMovieDetailClicked(movie: Result)             // Open move detail activity
    }
}