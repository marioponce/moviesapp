package com.dev.marioponce.moviesapp

import android.app.Application
import android.content.Context

class MoviesApplication : Application() {

    companion object {
        fun getApplicationContext() : Context {
            return getApplicationContext()
        }
    }
}