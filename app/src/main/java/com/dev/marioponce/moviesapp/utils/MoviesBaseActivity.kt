package com.dev.marioponce.moviesapp.utils

import com.dev.marioponce.moviesapp.interactor.executor.impl.MainThreadImpl
import java.util.concurrent.Executor
import java.util.concurrent.Executors

abstract class MoviesBaseInteractor {
    val mainThreadImpl : MainThreadImpl = MainThreadImpl()
    val executor : Executor = Executors.newSingleThreadExecutor()
}